/*
  BSD 2-Clause License

  Copyright (c) 2021, Masahiko KAWAGISHI
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.gitlab.mkwnana.s4firp;

import ij.*;
import ij.process.*;
import ij.gui.*;
import java.awt.*;
import ij.plugin.filter.*;

public class Search_for_4fIRP implements ExtendedPlugInFilter {
    ImagePlus imp;
    String arg;
    String titleBase;
    int flags = DOES_8G | DOES_16 | DOES_32 | DOES_RGB;
    int xCenter = 10;
    int yCenter = 10;
    int radiusMin = 1;
    int radiusMax = 10;
    int numPoints = 10;
    double[] weightChannel;
    String[] aryStrOutputType = {
        "One result for min - max", "One result for each radius",
        "Cumulative from min", "Cumulative from max", "Show Kernels"};
    int ONE_FOR_MIN_TO_MAX = 0, ONE_FOR_EACH_RADIUS = 1;
    int CUMULATIVE_FROM_MIN = 2, CUMULATIVE_FROM_MAX = 3;
    int SHOW_KERNELS = 4;
    int outputType;
    String saveDirParent = IJ.getDirectory("current");
    String saveDir = saveDirParent;

    public int setup(String arg, ImagePlus imp) {
        // GenericDialog.addDirectoryField() method introduced in
        // version 1.53d is required.
        if (IJ.versionLessThan("1.53d")) {
            return DONE;
        }
        this.arg = arg;
        this.imp = imp;
        if (imp == null) {
            return flags;
        }
        this.titleBase = imp.getTitle();
        int indPeriod = this.titleBase.lastIndexOf('.');
        if (0 < indPeriod) {
            this.titleBase = this.titleBase.substring(0, indPeriod);
        }
        return flags;
    }

    public int showDialog(ImagePlus imp, java.lang.String command,
                   PlugInFilterRunner  pfr) {
        if (arg.equals("one_center")) {
            int flagOnePoint = showDialogOnePoint();
            return flags | flagOnePoint;
        } else if (arg.equals("search_old")) {
            int flagSearchOld = showDialogSearch_old();
            return flags | flagSearchOld;
        } else if (arg.equals("search")) {
            int flagSearch = showDialogSearch();
            return flags | flagSearch;
        }
        IJ.log("Unknown arg: [" + arg + "]");
        return DONE;
    }

    public void run(ImageProcessor ip) {
        if (arg.equals("one_center")) {
            if (this.imp.getType() == ImagePlus.COLOR_RGB
                       || (this.imp.isComposite()
                           && 1 < this.imp.getNChannels())) {
                analyze_one_center_channels(ip);
            } else if (this.imp.getType() == ImagePlus.GRAY8
                || this.imp.getType() == ImagePlus.GRAY16
                || this.imp.getType() == ImagePlus.GRAY32) {
                analyze_one_center(this.imp, ip);
            }
        } else if (arg.equals("search_old")) {
            search_for_4fIRP_center_old(imp, this.radiusMin,
                                        this.radiusMax);
        } else if (arg.equals("search")) {
            search_for_4fIRP_center(imp, this.radiusMin,
                                    this.radiusMax);
        } else {
            IJ.log("Unknown arg: [" + arg + "]");
        }
   }

    public void setNPasses(int nPasses) {
    }

    int showDialogOnePoint() {
        GenericDialog gd = new GenericDialog("Analyze One Center of "
                                             + "Symmetry");
        gd.addMessage("This command does:");
        gd.addMessage("  Bicubic interpolation at polar grid with the"
                      + " center and min and max radius specified "
                      + "below.");
        gd.addMessage("  Fourier transform, power spectrum, and "
                      + "autocorrelation in the rotational "
                      + "direction.");
        gd.addNumericField("X Center", xCenter, 0);
        gd.addNumericField("Y Center", yCenter, 0);
        gd.addNumericField("Min Radius ", radiusMin, 0);
        gd.addNumericField("Max Radius ", radiusMax, 0);
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            if (this.weightChannel == null
                || this.weightChannel.length < 3) {
                this.weightChannel = new double[]{1.0, -1.0, 0};
            }
            gd.addMessage("Weights to combine RGB into grayscale.");
            gd.addNumericField("Red", this.weightChannel[0], 3);
            gd.addNumericField("Green", this.weightChannel[1], 3);
            gd.addNumericField("Blue", this.weightChannel[2], 3);
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            if (this.weightChannel == null
                || (this.weightChannel.length <
                    this.imp.getNChannels())) {
                this.weightChannel =
                    new double[this.imp.getNChannels()];
                this.weightChannel[0] = 1.0;
                this.weightChannel[1] = -1.0;
                for (int i = 2; i < this.imp.getNChannels(); i++) {
                    this.weightChannel[i] = 0;
                }
            }
            gd.addMessage("Weights to combine each channel into "
                          + "grayscale.");
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                gd.addNumericField
                    ("Channel" + (i + 1), this.weightChannel[i], 3);
            }
        }
        gd.addDirectoryField("Directory to create the results " +
                             "directory in: ",
                             IJ.getDirectory("current"));
        gd.addMessage("Warning: Files in the directory may be "
                      + "overwritten.");
        gd.showDialog();
        if (gd.wasCanceled()) {
            return DONE;
        }
        this.xCenter = (int) gd.getNextNumber();
        this.yCenter = (int) gd.getNextNumber();
        this.radiusMin = (int) gd.getNextNumber();
        this.radiusMax = (int) gd.getNextNumber();
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            for (int i = 0; i < 3; i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        }
        String saveDirParent = gd.getNextString();
        if (saveDirParent == null) {
            return DONE;
        }
        if (! new java.io.File(saveDirParent).isDirectory()) {
            return DONE;
        }
        this.saveDirParent = saveDirParent;
        this.saveDir = saveDirParent;
        return flags;
    }

    int showDialogSearch_old() {
        GenericDialog gd = new GenericDialog("Search For Possible "
                                             + "Center of Symmetry");
        gd.addMessage("This command searches for possible centers of "
                      + "4fIR-like symmetric pattern inside the ROI:"
                      );
        gd.addNumericField("Min Radius", radiusMin, 0);
        gd.addNumericField("Max Radius", radiusMax, 0);
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            if (this.weightChannel == null
                || this.weightChannel.length < 3) {
                this.weightChannel = new double[]{1.0, -1.0, 0};
            }
            gd.addMessage("Weights to combine RGB into grayscale.");
            gd.addNumericField("Red", this.weightChannel[0], 3);
            gd.addNumericField("Green", this.weightChannel[1], 3);
            gd.addNumericField("Blue", this.weightChannel[2], 3);
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            if (this.weightChannel == null
                || (this.weightChannel.length <
                    this.imp.getNChannels())) {
                this.weightChannel =
                    new double[this.imp.getNChannels()];
                this.weightChannel[0] = 1.0;
                this.weightChannel[1] = -1.0;
                for (int i = 2; i < this.imp.getNChannels(); i++) {
                    this.weightChannel[i] = 0;
                }
            }
            gd.addMessage("Weights to combine each channel into "
                          + "grayscale.");
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                gd.addNumericField
                    ("Channel" + (i + 1), this.weightChannel[i], 3);
            }
        }
        gd.addDirectoryField("Directory to create the results " +
                             "directory in: ",
                             IJ.getDirectory("current"));
        gd.addMessage("Warning: Files in the directory may be "
                      + "overwritten.");
        gd.showDialog();
        if (gd.wasCanceled()) {
            return DONE;
        }
        this.radiusMin = (int) gd.getNextNumber();
        this.radiusMax = (int) gd.getNextNumber();
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            for (int i = 0; i < 3; i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        }
        String saveDirParent = gd.getNextString();
        if (saveDirParent == null) {
            return DONE;
        }
        if (! new java.io.File(saveDirParent).isDirectory()) {
            return DONE;
        }
        this.saveDirParent = saveDirParent;
        this.saveDir = saveDirParent;
        return flags;
    }

    int showDialogSearch() {
        GenericDialog gd = new GenericDialog("Search For Possible "
                                             + "Center of Symmetry");
        gd.addMessage("This command searches for possible centers of "
                      + "4fIR-like symmetric pattern inside the ROI:"
                      );
        gd.addNumericField("Min Radius", radiusMin, 0);
        gd.addNumericField("Max Radius", radiusMax, 0);
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            if (this.weightChannel == null
                || this.weightChannel.length < 3) {
                this.weightChannel = new double[]{1.0, -1.0, 0};
            }
            gd.addMessage("Weights to combine RGB into grayscale.");
            gd.addNumericField("Red", this.weightChannel[0], 3);
            gd.addNumericField("Green", this.weightChannel[1], 3);
            gd.addNumericField("Blue", this.weightChannel[2], 3);
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            if (this.weightChannel == null
                || (this.weightChannel.length <
                    this.imp.getNChannels())) {
                this.weightChannel =
                    new double[this.imp.getNChannels()];
                this.weightChannel[0] = 1.0;
                this.weightChannel[1] = -1.0;
                for (int i = 2; i < this.imp.getNChannels(); i++) {
                    this.weightChannel[i] = 0;
                }
            }
            gd.addMessage("Weights to combine each channel into "
                          + "grayscale.");
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                gd.addNumericField
                    ("Channel" + (i + 1), this.weightChannel[i], 3);
            }
        }
        gd.addChoice("Output type", this.aryStrOutputType,
                     this.aryStrOutputType[this.ONE_FOR_MIN_TO_MAX]);
        gd.addDirectoryField("Directory to create the results " +
                             "directory in: ",
                             IJ.getDirectory("current"));
        gd.addMessage("Warning: Files in the directory may be "
                      + "overwritten.");
        gd.showDialog();
        if (gd.wasCanceled()) {
            return DONE;
        }
        this.radiusMin = (int) gd.getNextNumber();
        this.radiusMax = (int) gd.getNextNumber();
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            for (int i = 0; i < 3; i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        } else if (this.imp.isComposite()
            && 1 < this.imp.getNChannels()) {
            for (int i = 0; i < this.imp.getNChannels(); i++) {
                this.weightChannel[i] = gd.getNextNumber();
            }
        }
        this.outputType = gd.getNextChoiceIndex();
        String saveDirParent = gd.getNextString();
        if (saveDirParent == null) {
            return DONE;
        }
        if (! new java.io.File(saveDirParent).isDirectory()) {
            return DONE;
        }
        this.saveDirParent = saveDirParent;
        this.saveDir = saveDirParent;
        return flags;
    }

    double[][] interpolate_polar_grid(ImageProcessor iProcessor,
                                      int xCenter, int yCenter,
                                      int radiusMin, int radiusMax) {
        int numPoints = (int) Math.pow
            (2,
             Math.ceil(Math.log(2 * Math.PI * radiusMax)
                       / Math.log(2)));
        double[][] rv =
            new double[radiusMax - radiusMin + 1][numPoints];
        for (int i = 0; i <= radiusMax - radiusMin; i++) {
            for (int j = 0; j < numPoints; j++) {
                rv[i][j] = iProcessor.getBicubicInterpolatedPixel
                    (xCenter
                     - (radiusMin + i)
                     * Math.sin(2 * Math.PI * j / numPoints),
                     yCenter
                     - (radiusMin + i)
                     * Math.cos(2 * Math.PI * j / numPoints),
                     iProcessor);
            }
        }
        return rv;
    }

    float[][] fht_polar_grid(double[][] aryPolGrid) {
        float[] aryflRow = new float[aryPolGrid[0].length];
        FHT fht = new FHT();
        float[][] rv =
            new float[aryPolGrid.length][aryPolGrid[0].length];
        for (int i = 0; i < aryPolGrid.length; i++) {
            for (int j = 0; j < aryPolGrid[0].length; j++) {
                aryflRow[j] = (float) aryPolGrid[i][j];
            }
            fht.transform1D(aryflRow);
            for (int j = 0; j < aryPolGrid[0].length; j++) {
                rv[i][j] = aryflRow[j];
            }
        }
        return rv;
    }

    float[][] fft_real_polar_grid(float[][] aryPolGridFht) {
        float[][] rv =
            new float[aryPolGridFht.length][aryPolGridFht[0].length];
        for (int i = 0; i < aryPolGridFht.length; i++) {
            rv[i][0] = aryPolGridFht[i][0];
            for (int j = 1; j < aryPolGridFht[0].length; j++) {
                rv[i][j] =
                (aryPolGridFht[i][j]
                 + aryPolGridFht[i][aryPolGridFht[0].length - j]) / 2;
            }
        }
        return rv;
    }

    float[][] fft_imag_polar_grid(float[][] aryPolGridFht) {
        float[][] rv =
            new float[aryPolGridFht.length][aryPolGridFht[0].length];
        for (int i = 0; i < aryPolGridFht.length; i++) {
            rv[i][0] = 0;
            for (int j = 1; j < aryPolGridFht[0].length; j++) {
                rv[i][j] =
                (aryPolGridFht[i][aryPolGridFht[0].length - j]
                 - aryPolGridFht[i][j]) / 2;
            }
        }
        return rv;
    }

    float[][] power_spectrum_polar_grid(float[][] aryPolGridFht) {
        float[][] rv =
            new float[aryPolGridFht.length][aryPolGridFht[0].length];
        for (int i = 0; i < aryPolGridFht.length; i++) {
            rv[i][0] = (float) Math.pow(aryPolGridFht[i][0], 2);
            for (int j = 1; j < aryPolGridFht[0].length; j++) {
                rv[i][j] = (float)
                    (Math.pow(aryPolGridFht[i][j], 2)
                     + Math.pow
                     (aryPolGridFht[i][aryPolGridFht[0].length - j],
                      2))
                    / 2;
            }
        }
        return rv;
    }

    float[][] fft_amp_polar_grid(float[][] aryPolGridFht) {
        float[][] aryPS = power_spectrum_polar_grid(aryPolGridFht);
        float[][] rv =
            new float[aryPolGridFht.length][aryPolGridFht[0].length];
        for (int i = 0; i < aryPolGridFht.length; i++) {
            rv[i][0] = Math.abs(aryPolGridFht[i][0]);
            for (int j = 1; j < aryPolGridFht[0].length; j++) {
                rv[i][j] = (float) Math.sqrt(aryPS[i][j]);
            }
        }
        return rv;
    }

    float[][] fft_phase_polar_grid(float[][] aryPolGridFht) {
        float[][] aryReal = fft_real_polar_grid(aryPolGridFht);
        float[][] aryImag = fft_imag_polar_grid(aryPolGridFht);
        float[][] rv =
            new float[aryPolGridFht.length][aryPolGridFht[0].length];
        for (int i = 0; i < aryPolGridFht.length; i++) {
            rv[i][0] = 0;
            for (int j = 1; j < aryPolGridFht[0].length; j++) {
                rv[i][j] = (float) Math.atan2(aryImag[i][j],
                                              aryReal[i][j]);
            }
        }
        return rv;
    }

    float[] fft_sum_amp_4fIRP_polar_grid(float[][] aryPolGridFht) {
        float[][] aryAmp = fft_amp_polar_grid(aryPolGridFht);
        float[] rv =
            new float[aryPolGridFht.length];
        rv[0] = aryAmp[0][2];
        for (int i = 1; i < aryPolGridFht.length; i++) {
            rv[i] = rv[i - 1] + aryAmp[i][2];
        }
        return rv;
    }

    float[][] inv_fht_power_spec_polar_grid(float[][] aryPolGridPS) {
        float[] aryflRow = new float[aryPolGridPS[0].length];
        FHT fht = new FHT();
        float[][] rv =
            new float[aryPolGridPS.length][aryPolGridPS[0].length];
        for (int i = 0; i < aryPolGridPS.length; i++) {
            for (int j = 0; j < aryPolGridPS[0].length; j++) {
                aryflRow[j] = aryPolGridPS[i][j];
            }
            fht.inverseTransform1D(aryflRow);
            for (int j = 0; j < aryPolGridPS[0].length; j++) {
                rv[i][j] = aryflRow[j];
            }
        }
        return rv;
    }

    float[][] auto_corr_polar_grid(float[][] aryPolGridPSFht) {
        float[][] rv =
            new float
            [aryPolGridPSFht.length][aryPolGridPSFht[0].length];
        for (int i = 0; i < aryPolGridPSFht.length; i++) {
            rv[i][0] = aryPolGridPSFht[i][0];
            for (int j = 1; j < aryPolGridPSFht[0].length; j++) {
                rv[i][j] =
                (aryPolGridPSFht[i][j]
                 + aryPolGridPSFht[i][aryPolGridPSFht[0].length - j])
                    / 2;
            }
        }
        return rv;
    }

    float[][] auto_corr_norm_polar_grid(float[][] aryPolGridAC) {
        float[][] rv =
            new float
            [aryPolGridAC.length][aryPolGridAC[0].length];
        for (int i = 0; i < aryPolGridAC.length; i++) {
            rv[i][0] = 1;
            for (int j = 1; j < aryPolGridAC[0].length; j++) {
                rv[i][j] = aryPolGridAC[i][j] / aryPolGridAC[i][0];
            }
        }
        return rv;
    }

    ImagePlus combine_channels_to_gray
        (ImagePlus impChannels, double[] weightChannel) {
        ImagePlus[] aryImpChannel =
            ij.plugin.ChannelSplitter.split(impChannels);
        if (aryImpChannel.length != weightChannel.length) {
            IJ.log("BUG!!! Number of channels: "
                   + aryImpChannel.length + " != Number of weights: "
                   + weightChannel.length);
        }
        ImageStack[] aryImgStackForChannel =
            new ImageStack[aryImpChannel.length];
        for (int i = 0; i < aryImpChannel.length; i++) {
            aryImgStackForChannel[i] =
                new ImageStack(impChannels.getWidth(),
                               impChannels.getHeight(),
                               aryImpChannel[0].getStackSize());
            for (int j = 0; j < aryImpChannel[0].getStackSize(); j++
                 ) {
                FloatProcessor fpChannel
                    = aryImpChannel[i].getImageStack()
                    .getProcessor(j + 1).convertToFloatProcessor();
                fpChannel.multiply(weightChannel[i]);
                aryImgStackForChannel[i].setProcessor(fpChannel,
                                                      j + 1);
            }
        }
        ij.plugin.ImageCalculator ic =
            new ij.plugin.ImageCalculator();
        ImagePlus rv = ic.run("Add create 32-bit stack",
                              new ImagePlus("", aryImgStackForChannel[0]),
                              new ImagePlus("", aryImgStackForChannel[1]));
        for (int i = 2; i < aryImpChannel.length; i++) {
            rv = ic.run("Add create 32-bit stack",
                        rv, new ImagePlus("", aryImgStackForChannel[i]));
        }
        rv.getProcessor().setColorModel(null);
        String combinedTitle = this.titleBase + "_combined";
        for (int i = 0; i < weightChannel.length; i++) {
            combinedTitle = combinedTitle + "_" + weightChannel[i];
        }
        rv.setTitle(combinedTitle);
        return rv;
    }

    void analyze_one_center_channels(ImageProcessor ip) {
        ImagePlus[] aryImpChannel =
            ij.plugin.ChannelSplitter.split(this.imp);
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            aryImpChannel[0].setTitle(this.titleBase + "_Red");
            aryImpChannel[1].setTitle(this.titleBase + "_Green");
            aryImpChannel[2].setTitle(this.titleBase + "_Blue");
        } else if (this.imp.isComposite()
                   && 1 < this.imp.getNChannels()) {
            for (int i = 0; i < aryImpChannel.length; i++) {
                aryImpChannel[i].setTitle(this.titleBase + "_channel"
                                          + (i + 1));
            }
        }
        ImageProcessor[] aryIpChannels =
            new ImageProcessor[aryImpChannel.length];
        for (int i = 0; i < aryImpChannel.length; i++) {
            aryIpChannels[i] = aryImpChannel[i].getProcessor();
        }
        if (this.imp.getType() == ImagePlus.COLOR_RGB) {
            check_red_equals_blue(aryIpChannels[0], aryIpChannels[2]);
        }
        ImagePlus impCombined =
            combine_channels_to_gray(this.imp, this.weightChannel);
        for (int i = 0; i < aryImpChannel.length; i++) {
            analyze_one_center(aryImpChannel[i], aryIpChannels[i]);
        }
        analyze_one_center(impCombined, impCombined.getProcessor());
    }

    void check_red_equals_blue(ImageProcessor ipRed,
                               ImageProcessor ipBlue) {
        boolean RandBdiffers = false;
        for (int i = 0; i < ipRed.getWidth(); i++) {
            for (int j = 0; j < ipRed.getHeight(); j++) {
                if (ipRed.getPixelValue(i, j) !=
                    ipBlue.getPixelValue(i, j)) {
                    IJ.log("Red and Blue Channels differ. for example"
                           + " at (" + i + ", " + j + ") Red: "
                           + ipRed.getPixelValue(i, j) + " vs Blue: "
                           + ipBlue.getPixelValue(i, j));
                    RandBdiffers = true;
                    break;
                }
            }
            if (RandBdiffers) {
                break;
            }
        }
    }

    void save_ary_as_img(float[][] aryData, String title) {
        IJ.log("Saving image: " + title);
        ImagePlus impData = IJ.createImage(title, "32-bit black",
                                            aryData[0].length,
                                            aryData.length, 1);
        ImageProcessor ipData = impData.getProcessor();
        for (int i = 0; i < aryData.length; i++) {
            for (int j = 0; j < aryData[0].length; j++) {
                ipData.putPixelValue(j, i, aryData[i][j]);
            }
        }
        ipData.resetMinAndMax();
        IJ.saveAs(impData, "tif", this.saveDir + title);
    }

    void save_ary_as_img(double[][] aryData, String title) {
        float[][] aryflData =
            new float[aryData.length][aryData[0].length];
        for (int i = 0; i < aryData.length; i++) {
            for (int j = 0; j < aryData[0].length; j++) {
                aryflData[i][j] = (float) aryData[i][j];
            }
        }
        save_ary_as_img(aryflData,  title);
    }

    void save_ary_as_table(float[][] aryData, String title) {
        IJ.log("Saving table: " + title);
        ij.measure.ResultsTable rtData =
            new ij.measure.ResultsTable();
        rtData.setPrecision(6);
        for (int i = 0; i < aryData.length; i++) {
            rtData.incrementCounter();
            rtData.addLabel("radius" + (this.radiusMin + i));
            for (int j = 0; j < aryData[0].length; j++) {
                rtData.addValue
                    ("phi" + (2 * j) + "over" + aryData[0].length
                     + "pi",
                     aryData[i][j]);
            }
        }
        rtData.save(this.saveDir + title + ".csv");
    }

    void save_ary_as_table(double[][] aryData, String title) {
        float[][] aryflData =
            new float[aryData.length][aryData[0].length];
        for (int i = 0; i < aryData.length; i++) {
            for (int j = 0; j < aryData[0].length; j++) {
                aryflData[i][j] = (float) aryData[i][j];
            }
        }
        save_ary_as_table(aryflData,  title);
    }

    void analyze_one_center(ImagePlus impSingle,
                            ImageProcessor ipSingle) {
        String titleBaseSingle = impSingle.getTitle();
        int indPeriod = titleBaseSingle.lastIndexOf('.');
        if (0 < indPeriod) {
            titleBaseSingle = titleBaseSingle.substring(0, indPeriod);
        }
        this.saveDir = this.saveDirParent + java.io.File.separator +
            titleBaseSingle + "_one_center" + java.io.File.separator;
        java.io.File fileSaveDir = new java.io.File(this.saveDir);
        if (fileSaveDir.isFile()) {
            IJ.log("Failed to make directory \"" + this.saveDir +
                   "\". A file already exists.");
            return;
        }
        fileSaveDir.mkdir();
        IJ.log("Saving single channel image: " + titleBaseSingle);
        IJ.run(impSingle, "Specify...",
               "width=" + (this.radiusMax * 2) + " height="
               + (this.radiusMax * 2) + " x="
               + (this.xCenter - this.radiusMax) + " y="
               + (this.yCenter - this.radiusMax) + " oval");
        IJ.run(impSingle, "Add Selection...", "");
        IJ.run(impSingle, "Specify...",
               "width=" + (this.radiusMin * 2) + " height="
               + (this.radiusMin * 2) + " x="
               + (this.xCenter - this.radiusMin) + " y="
               + (this.yCenter - this.radiusMin) + " oval");
        IJ.run(impSingle, "Add Selection...", "");
        IJ.saveAs(impSingle, "tif", this.saveDir + titleBaseSingle);
        double[][] aryPosGridSingle =
            interpolate_polar_grid(ipSingle, this.xCenter,
                                   this.yCenter, this.radiusMin,
                                   this.radiusMax);
        save_ary_as_img(aryPosGridSingle,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin
                        + "_" + this.radiusMax);
        save_ary_as_table(aryPosGridSingle,
                          titleBaseSingle + "_pol_grid_"
                          + this.xCenter + "_" + this.yCenter + "_"
                          + this.radiusMin + "_" + this.radiusMax);
        float[][] aryFht = fht_polar_grid(aryPosGridSingle);
        float[][] aryPS = power_spectrum_polar_grid(aryFht);
        save_ary_as_img(aryPS,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin
                        + "_" + this.radiusMax + "_ps");
        save_ary_as_table(aryPS,
                          titleBaseSingle + "_pol_grid_"+ this.xCenter
                          + "_" + this.yCenter + "_" + this.radiusMin
                          + "_" + this.radiusMax + "_ps");
        float[][] aryFftAmp = fft_amp_polar_grid(aryFht);
        save_ary_as_img(aryFftAmp,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin +
                        "_" + this.radiusMax + "_fft_amp");
        save_ary_as_table(aryFftAmp,
                          titleBaseSingle + "_pol_grid_"
                          + this.xCenter + "_" + this.yCenter + "_"
                          + this.radiusMin + "_" + this.radiusMax
                          + "_fft_amp");
        float[][] aryFftPhase = fft_phase_polar_grid(aryFht);
        save_ary_as_img(aryFftPhase,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin
                        + "_" + this.radiusMax + "_fft_phase");
        save_ary_as_table(aryFftPhase,
                          titleBaseSingle + "_pol_grid_"
                          + this.xCenter + "_" + this.yCenter + "_"
                          + this.radiusMin + "_" + this.radiusMax
                          + "_fft_phase");
        float[][] aryPSinvFht = inv_fht_power_spec_polar_grid(aryPS);
        float[][] aryAutoCorr = auto_corr_polar_grid(aryPSinvFht);
        save_ary_as_img(aryAutoCorr,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin
                        + "_" + this.radiusMax + "_auto_corr");
        save_ary_as_table(aryAutoCorr,
                          titleBaseSingle + "_pol_grid_"
                          + this.xCenter + "_" + this.yCenter + "_"
                          + this.radiusMin + "_" + this.radiusMax
                          + "_auto_corr");
        float[][] aryAutoCorrNorm =
            auto_corr_norm_polar_grid(aryAutoCorr);
        save_ary_as_img(aryAutoCorrNorm,
                        titleBaseSingle + "_pol_grid_" + this.xCenter
                        + "_" + this.yCenter + "_" + this.radiusMin
                        + "_" + this.radiusMax + "_auto_corr_norm");
        save_ary_as_table(aryAutoCorrNorm,
                          titleBaseSingle + "_pol_grid_"
                          + this.xCenter + "_" + this.yCenter + "_"
                          + this.radiusMin + "_" + this.radiusMax
                          + "_auto_corr_norm");
    }

    void search_for_4fIRP_center_old(ImagePlus impStack,
                                     int radiusMin, int radiusMax) {
        String titleBase = impStack.getTitle();
        int indPeriod = titleBase.lastIndexOf('.');
        if (0 < indPeriod) {
            titleBase = titleBase.substring(0, indPeriod);
        }
        this.saveDir = this.saveDirParent + java.io.File.separator +
            titleBase + "_search_old" + java.io.File.separator;
        java.io.File fileSaveDir = new java.io.File(this.saveDir);
        if (fileSaveDir.isFile()) {
            IJ.log("Failed to make directory \"" + this.saveDir +
                   "\". A file already exists.");
            return;
        }
        fileSaveDir.mkdir();
        Roi roiStack = impStack.getRoi();
        ImagePlus impCombined;
        if (impStack.getType() == ImagePlus.COLOR_RGB
            || (impStack.isComposite()
                && 1 < impStack.getNChannels())) {
            impCombined =
                combine_channels_to_gray(impStack,
                                         this.weightChannel);
        } else {
            impCombined = impStack;
        }
        ImageStack imgStackCombined = impCombined.getStack();
        int nStackSize = impCombined.getStackSize();
        float[][][][] aryFrameRadiusYXSumAmp =
            new float[nStackSize
                      ][radiusMax - radiusMin + 1
                        ][imgStackCombined.getHeight()
                          ][imgStackCombined.getWidth()];
        for (int i = 0; i < nStackSize; i++) {
            for (int j = 0; j < radiusMax - radiusMin + 1; j++) {
                for (int k = 0; k < imgStackCombined.getHeight();
                     k++) {
                    for (int l = 0; l < imgStackCombined.getWidth();
                         l++) {
                        aryFrameRadiusYXSumAmp[i][j][k][l] =
                            Float.NaN;
                    }
                }
            }
        }
        int numProcessors = Runtime.getRuntime().availableProcessors();
        Thread[] aryThreadsSearchInRow = new Thread[numProcessors];
        for (int i = 0; i < nStackSize; i++) {
            for (int j = 0; j < numProcessors; j++) {
                int[] aryRows =
                    new int[(imgStackCombined.getHeight() - 1 - j)
                            / numProcessors + 1];
                for (int k = 0; k < aryRows.length; k++) {
                    aryRows[k] = j + k * numProcessors;
                }
                int iEffectivelyFinal = i;
                aryThreadsSearchInRow[j] =
                    new Thread
                    (
                     () -> {
                         for (int k = 0; k < aryRows.length; k++) {
                             for (int l = 0;
                                  l < imgStackCombined.getWidth(); l++
                                  ) {
                                 if (roiStack == null ||
                                     roiStack.contains(l, aryRows[k])
                                     ) {
                                     double[][] aryPolGrid =
                                         interpolate_polar_grid
                                         (imgStackCombined.
                                          getProcessor
                                          (iEffectivelyFinal + 1),
                                          l, aryRows[k], radiusMin,
                                          radiusMax);
                                     float[][] aryFht =
                                         fht_polar_grid(aryPolGrid);
                                     float[] arySumAmp4fIRP =
                                         fft_sum_amp_4fIRP_polar_grid
                                         (aryFht);
                                     synchronized
                                         (aryFrameRadiusYXSumAmp) {
                                         for (int m = 0;
                                              m < radiusMax -
                                                  radiusMin + 1;
                                              m++) {
                                             aryFrameRadiusYXSumAmp
                                                 [iEffectivelyFinal
                                                  ][m][aryRows[k]][l]
                                                 = arySumAmp4fIRP[m];
                                         }
                                     }
                                 }
                             }
                         }
                     }
                     );
                aryThreadsSearchInRow[j].start();
            }
            for (int j = 0; j < numProcessors; j++) {
                try {
                    aryThreadsSearchInRow[j].join();
                } catch (InterruptedException e) {
                    IJ.log("Thread has already been interrupted: " + j);
                }
            }
        }
        ImagePlus impSumAmp =
            IJ.createImage
            ("4fIRP_Likely", "32-bit black composite-mode",
             imgStackCombined.getWidth(), imgStackCombined.getHeight(),
             1, radiusMax - radiusMin + 1, nStackSize);
        ImageStack imgStackSumAmp = impSumAmp.getStack();
        for (int i = 0; i < nStackSize; i++) {
            for (int j = 0; j <= radiusMax - radiusMin ; j++) {
                imgStackSumAmp.setSliceLabel
                    ("timeframe" + (i + 1) +"_radius" +
                     (this.radiusMin + j),
                     i * (radiusMax - radiusMin + 1) + j + 1);
                ImageProcessor ipSumAmp =
                    imgStackSumAmp.getProcessor
                    (i * (radiusMax - radiusMin + 1) + j + 1);
                for (int k = 0; k < impCombined.getHeight(); k++) {
                    for (int l = 0; l < impCombined.getWidth(); l++) {
                        if (k < j
                            || impCombined.getHeight() <= k + j
                            || l < j
                            || impCombined.getWidth() <= l + j
                            || (roiStack != null
                                && ! roiStack.contains(l, k))) {
                            ipSumAmp.putPixelValue
                                (l, k, Float.NaN);
                        } else {
                            ipSumAmp.putPixelValue
                                (l, k,
                                 aryFrameRadiusYXSumAmp[i][j][k][l]);
                        }
                    }
                }
            }
        }
        IJ.resetMinAndMax(impSumAmp);
        IJ.log("Saving image: " + impSumAmp.getTitle());
        IJ.saveAs(impSumAmp, "tif", this.saveDir + impSumAmp.getTitle());
        impSumAmp.show();
    }

    int get_size_padded(ImagePlus imp) {
        int sideLarger = Math.max(imp.getWidth(), imp.getHeight());
        int rv =
            (int) Math.round
            (Math.pow
             (2,
              Math.ceil(Math.log(sideLarger) / Math.log(2)) + 1));
        return rv;
    }

    ImageStack[] create_imgstack_complex_kernels
        (int aSize, int radiusMin, int radiusMax, int outputType) {
        int lengthOfKernels;
        if (outputType == ONE_FOR_MIN_TO_MAX) {
            lengthOfKernels = 1;
        } else if (outputType == ONE_FOR_EACH_RADIUS
                   || outputType == CUMULATIVE_FROM_MIN
                   || outputType == CUMULATIVE_FROM_MAX
                   || outputType == SHOW_KERNELS) {
            lengthOfKernels = radiusMax - radiusMin + 1;
        } else {
            IJ.log("Unknown outputType: " + outputType);
            return null;
        }
        ImageStack iStackReal =
            new ImageStack(aSize, aSize, lengthOfKernels);
        ImageStack iStackImag =
            new ImageStack(aSize, aSize, lengthOfKernels);
        for (int i = 0; i < lengthOfKernels; i++) {
            FloatProcessor fpReal = new FloatProcessor(aSize, aSize);
            FloatProcessor fpImag = new FloatProcessor(aSize, aSize);
            int yFromCenter, xFromCenter;
            double radiusFromCenter, phaseFromCenter;
            for (int j = 0; j < aSize; j++) {
                for (int k = 0; k < aSize; k++) {
                    yFromCenter = j - aSize / 2;
                    xFromCenter = k - aSize / 2;
                    radiusFromCenter =
                        Math.sqrt(yFromCenter * yFromCenter
                                  + xFromCenter * xFromCenter);
                    if ((outputType == ONE_FOR_MIN_TO_MAX
                         && (radiusFromCenter < radiusMin - 0.5
                             || radiusMax + 0.5 <= radiusFromCenter))
                        || (outputType == ONE_FOR_EACH_RADIUS
                            && (radiusFromCenter < radiusMin + i - 0.5
                                || radiusMin + i + 0.5 <=
                                radiusFromCenter))
                        || (outputType == CUMULATIVE_FROM_MIN
                            && (radiusFromCenter < radiusMin - 0.5
                                || radiusMin + i + 0.5 <=
                                radiusFromCenter))
                        || (outputType == CUMULATIVE_FROM_MAX
                            && (radiusFromCenter < radiusMin + i - 0.5
                                || radiusMax + 0.5 <=
                                radiusFromCenter))) {
                        fpReal.putPixelValue(k, j, 0);
                        fpImag.putPixelValue(k, j, 0);
                        continue;
                    }
                    phaseFromCenter = Math.atan2(xFromCenter,
                                             yFromCenter);
                    if (outputType == ONE_FOR_MIN_TO_MAX) {
                        fpReal.putPixelValue
                            (k, j,
                             Math.cos(-2 * phaseFromCenter)
                             / (radiusFromCenter
                                * (radiusMax - radiusMin + 1)));
                        fpImag.putPixelValue
                            (k, j,
                             Math.sin(-2 * phaseFromCenter)
                             / (radiusFromCenter
                                * (radiusMax - radiusMin + 1)));
                    } else if (outputType == ONE_FOR_EACH_RADIUS) {
                        fpReal.putPixelValue
                            (k, j,
                             Math.cos(-2 * phaseFromCenter)
                             / radiusFromCenter);
                        fpImag.putPixelValue
                            (k, j,
                             Math.sin(-2 * phaseFromCenter)
                             / radiusFromCenter);
                    } else if (outputType == CUMULATIVE_FROM_MIN) {
                        fpReal.putPixelValue
                            (k, j,
                             Math.cos(-2 * phaseFromCenter)
                             / (radiusFromCenter * (i + 1)));
                        fpImag.putPixelValue
                            (k, j,
                             Math.sin(-2 * phaseFromCenter)
                             / (radiusFromCenter * (i + 1)));
                    } else if (outputType == CUMULATIVE_FROM_MAX) {
                        fpReal.putPixelValue
                            (k, j,
                             Math.cos(-2 * phaseFromCenter)
                             / (radiusFromCenter
                                * (radiusMax - radiusMin - i + 1)));
                        fpImag.putPixelValue
                            (k, j,
                             Math.sin(-2 * phaseFromCenter)
                             / (radiusFromCenter
                                * (radiusMax - radiusMin - i + 1)));
                    }
                }
            }
            iStackReal.setProcessor(fpReal, i + 1);
            iStackImag.setProcessor(fpImag, i + 1);
        }
        ImageStack[] rv = {iStackReal, iStackImag};
        return rv;
    }

    void show_error_msg() {
        int sizePadded = get_size_padded(this.imp);
        int nStacks = this.imp.getNSlices() * this.imp.getNFrames();
        IJ.log("Failed to perform operation.");
        IJ.log("java.lang.OutOfMemoryError is suspected.");
        IJ.log("Go to Edit > Options > Misc... and ");
        IJ.log("set \"Debug mode\" to [+] and try this plug-in"
               + " again.");
        IJ.log("Check the output log for any occurrence of "
               + "java.lang.OutOfMemoryError .");
        IJ.log("This operation requires memory of roughly at least");
        IJ.log("4 bytes * s^2 * (p + 1) * (k + 1) = "
               + (4.0 * sizePadded * sizePadded
                  * (nStacks + 1)
                  * (radiusMax - radiusMin + 2) / 0x100000)
               + " MiB.");
        IJ.log("where s: Image size padded to  power of 2: "
               + sizePadded);
        IJ.log("p: Number of slices of image: "
               + nStacks);
        IJ.log("k: Number of radii: "
               + (radiusMax - radiusMin + 1));
    }

    void search_for_4fIRP_center(ImagePlus impOriginal, int radiusMin,
                                 int radiusMax) {
        String titleBase = impOriginal.getTitle();
        int indPeriod = titleBase.lastIndexOf('.');
        if (0 < indPeriod) {
            titleBase = titleBase.substring(0, indPeriod);
        }
        this.saveDir = this.saveDirParent + java.io.File.separator +
            titleBase;
        if (this.outputType == ONE_FOR_MIN_TO_MAX) {
            this.saveDir += "_one_for_min_to_max" +
                java.io.File.separator;
        } else if (this.outputType == ONE_FOR_EACH_RADIUS) {
            this.saveDir += "_one_for_each_radius" +
                java.io.File.separator;
        } else if (this.outputType == CUMULATIVE_FROM_MIN) {
            this.saveDir += "_cumulative_from_min" +
                java.io.File.separator;
        } else if (this.outputType == CUMULATIVE_FROM_MAX) {
            this.saveDir += "_cumulative_from_max" +
                java.io.File.separator;
        } else if (this.outputType == SHOW_KERNELS) {
            this.saveDir += "_show_kernels" + java.io.File.separator;
        }
        java.io.File fileSaveDir = new java.io.File(this.saveDir);
        if (fileSaveDir.isFile()) {
            IJ.log("Failed to make directory \"" + this.saveDir +
                   "\". A file already exists.");
            return;
        }
        fileSaveDir.mkdir();
        Roi roiStack = impOriginal.getRoi();
        ImagePlus impCombined;
        if (impOriginal.getType() == ImagePlus.COLOR_RGB
            || (impOriginal.isComposite()
                && 1 < impOriginal.getNChannels())) {
            impCombined =
                combine_channels_to_gray(impOriginal,
                                         this.weightChannel);
        } else {
            impCombined = impOriginal;
        }
        int sizePadded = get_size_padded(impCombined);
        int yOffset = (sizePadded - impCombined.getHeight()) / 2;
        int xOffset = (sizePadded - impCombined.getWidth()) / 2;
        ImageStack imgStackCombined = impCombined.getStack();
        int nStacksCombined = imgStackCombined.getSize();
        FHT[] aryFhtPadded = new FHT[nStacksCombined];
        int numProcessors = Runtime.getRuntime().availableProcessors();
        Thread[] aryThreadsConvolPadded = new Thread[numProcessors];
        for (int i = 0; i < numProcessors; i++) {
            int[] aryInd = new int[Math.floorDiv
                                   (nStacksCombined - 1 - i,
                                    numProcessors) + 1];
            for (int j = 0; j < aryInd.length; j++) {
                    aryInd[j] = i + j * numProcessors;
                }
            aryThreadsConvolPadded[i] = new Thread
                (() -> {
                    for (int k = 0; k < aryInd.length; k++) {
                        FloatProcessor fpTemp =
                            new FloatProcessor(sizePadded,
                                               sizePadded);
                        if (imgStackCombined.getWidth() == sizePadded
                            || imgStackCombined.getHeight() ==
                            sizePadded) {
                            fpTemp.insert(imgStackCombined.
                                          getProcessor(aryInd[k] + 1),
                                          0, 0);
                        } else {
                            fpTemp.setValue(0);
                            fpTemp.fill();
                            fpTemp.insert(imgStackCombined.
                                          getProcessor(aryInd[k] + 1),
                                          xOffset, yOffset);
                        }
                        FHT fhtTemp = new FHT(fpTemp);
                        fhtTemp.transform();
                        synchronized (aryFhtPadded) {
                            aryFhtPadded[aryInd[k]] = fhtTemp;
                        }
                    }
                });
            aryThreadsConvolPadded[i].start();
        }
        for (int i = 0; i < numProcessors; i++) {
            try {
                aryThreadsConvolPadded[i].join();
            } catch (InterruptedException e) {
                IJ.log("Thread has already been interrupted: " + i);
            }
        }
        boolean aryFhtPaddedContainsNull = false;
        for (FHT fhtOne : aryFhtPadded) {
            if (fhtOne == null) {
                aryFhtPaddedContainsNull = true;
                break;
            }
        }
        if (aryFhtPaddedContainsNull) {
            IJ.log("Failed to perform FHT of image (stack).");
            show_error_msg();
            return;
        }
        if (this.outputType == SHOW_KERNELS) {
            ImageStack[] istack = create_imgstack_complex_kernels
                (sizePadded, radiusMin, radiusMax,
                 this.ONE_FOR_EACH_RADIUS);
            ImagePlus impKernelOneForEachReal =
                new ImagePlus("KernelForEachRadiusReal" + radiusMin
                              + "-" + radiusMax,
                              istack[0]);
            ImagePlus impKernelOneForEachImag =
                new ImagePlus("KernelForEachRadiusImag" + radiusMin
                              + "-" + radiusMax,
                              istack[1]);
            istack = create_imgstack_complex_kernels
                (sizePadded, radiusMin, radiusMax,
                 this.CUMULATIVE_FROM_MIN);
            ImagePlus impKernelCumulFromMinReal =
                new ImagePlus("KernelCumulFromMinReal" + radiusMin
                              + "-" + radiusMax,
                              istack[0]);
            ImagePlus impKernelCumulFromMinImag =
                new ImagePlus("KernelCumulFromMinImag" + radiusMin
                              + "-" + radiusMax,
                              istack[1]);
            istack = create_imgstack_complex_kernels
                (sizePadded, radiusMin, radiusMax,
                 this.CUMULATIVE_FROM_MAX);
            ImagePlus impKernelCumulFromMaxReal =
                new ImagePlus("KernelCumulFromMaxReal" + radiusMin
                              + "-" + radiusMax,
                              istack[0]);
            ImagePlus impKernelCumulFromMaxImag =
                new ImagePlus("KernelCumulFromMaxImag" + radiusMin
                              + "-" + radiusMax,
                              istack[1]);
            IJ.log("Saving image: " +
                   impKernelOneForEachReal.getTitle());
            IJ.saveAs(impKernelOneForEachReal, "tif",
                      this.saveDir +
                      impKernelOneForEachReal.getTitle());
            IJ.log("Saving image: " +
                   impKernelOneForEachImag.getTitle());
            IJ.saveAs(impKernelOneForEachImag, "tif",
                      this.saveDir +
                      impKernelOneForEachImag.getTitle());
            IJ.log("Saving image: " +
                   impKernelCumulFromMinReal.getTitle());
            IJ.saveAs(impKernelCumulFromMinReal, "tif",
                      this.saveDir +
                      impKernelCumulFromMinReal.getTitle());
            IJ.log("Saving image: " +
                   impKernelCumulFromMinImag.getTitle());
            IJ.saveAs(impKernelCumulFromMinImag, "tif",
                      this.saveDir +
                      impKernelCumulFromMinImag.getTitle());
            IJ.log("Saving image: " +
                   impKernelCumulFromMaxReal.getTitle());
            IJ.saveAs(impKernelCumulFromMaxReal, "tif",
                      this.saveDir +
                      impKernelCumulFromMaxReal.getTitle());
            IJ.log("Saving image: " +
                   impKernelCumulFromMaxImag.getTitle());
            IJ.saveAs(impKernelCumulFromMaxImag, "tif",
                      this.saveDir +
                      impKernelCumulFromMaxImag.getTitle());
            impKernelOneForEachReal.show();
            impKernelOneForEachImag.show();
            impKernelCumulFromMinReal.show();
            impKernelCumulFromMinImag.show();
            impKernelCumulFromMaxReal.show();
            impKernelCumulFromMaxImag.show();
            return;
        }
        ImageStack[] iStackKernels =
            create_imgstack_complex_kernels
            (sizePadded, radiusMin, radiusMax, this.outputType);
        ImageStack iStackKernelReal = iStackKernels[0];
        ImageStack iStackKernelImag = iStackKernels[1];
        FHT[] aryFhtKernelReal = new FHT[iStackKernelReal.getSize()];
        FHT[] aryFhtKernelImag = new FHT[iStackKernelReal.getSize()];
        Thread[] aryThreadsConvolKernelReal =
            new Thread[numProcessors];
        Thread[] aryThreadsConvolKernelImag =
            new Thread[numProcessors];
        for (int i = 0; i < numProcessors; i++) {
            int[] aryInd =
                new int[Math.floorDiv
                        (iStackKernelReal.getSize() - 1 - i,
                         numProcessors) + 1];
            for (int j = 0; j < aryInd.length; j++) {
                    aryInd[j] = i + j * numProcessors;
                }
            aryThreadsConvolKernelReal[i] = new Thread
                (() -> {
                    for (int k = 0; k < aryInd.length; k++) {
                        FloatProcessor fpTemp =
                            new FloatProcessor(sizePadded,
                                               sizePadded);
                        if (iStackKernelReal.getWidth() == sizePadded
                            || iStackKernelReal.getHeight() ==
                            sizePadded) {
                            fpTemp.insert(iStackKernelReal.
                                          getProcessor(aryInd[k] + 1),
                                          0, 0);
                        } else {
                            fpTemp.setValue(0);
                            fpTemp.fill();
                            fpTemp.insert(iStackKernelReal.
                                          getProcessor(aryInd[k] + 1),
                                          xOffset, yOffset);
                        }
                        FHT fhtTemp = new FHT(fpTemp);
                        fhtTemp.transform();
                        synchronized (aryFhtKernelReal) {
                            aryFhtKernelReal[aryInd[k]] = fhtTemp;
                        }
                    }
                });
            aryThreadsConvolKernelImag[i] = new Thread
                (() -> {
                    for (int k = 0; k < aryInd.length; k++) {
                        FloatProcessor fpTemp =
                            new FloatProcessor(sizePadded,
                                               sizePadded);
                        if (iStackKernelImag.getWidth() == sizePadded
                            || iStackKernelImag.getHeight() ==
                            sizePadded) {
                            fpTemp.insert(iStackKernelImag.
                                          getProcessor(aryInd[k] + 1),
                                          0, 0);
                        } else {
                            fpTemp.setValue(0);
                            fpTemp.fill();
                            fpTemp.insert(iStackKernelImag.
                                          getProcessor(aryInd[k] + 1),
                                          xOffset, yOffset);
                        }
                        FHT fhtTemp = new FHT(fpTemp);
                        fhtTemp.transform();
                        synchronized (aryFhtKernelImag) {
                            aryFhtKernelImag[aryInd[k]] = fhtTemp;
                        }
                    }
                });
            aryThreadsConvolKernelReal[i].start();
            aryThreadsConvolKernelImag[i].start();
        }
        for (int i = 0; i < numProcessors; i++) {
            try {
                aryThreadsConvolKernelReal[i].join();
            } catch (InterruptedException e) {
                IJ.log("Thread has already been interrupted: " + i);
            }
        }
        for (int i = 0; i < numProcessors; i++) {
            try {
                aryThreadsConvolKernelImag[i].join();
            } catch (InterruptedException e) {
                IJ.log("Thread has already been interrupted: " + i);
            }
        }
        boolean aryFhtKernelRealContainsNull = false;
        boolean aryFhtKernelImagContainsNull = false;
        for (FHT fhtOne : aryFhtKernelReal) {
            if (fhtOne == null) {
                aryFhtKernelRealContainsNull = true;
                break;
            }
        }
        for (FHT fhtOne : aryFhtKernelImag) {
            if (fhtOne == null) {
                aryFhtKernelImagContainsNull = true;
                break;
            }
        }
        if (aryFhtKernelRealContainsNull ||
            aryFhtKernelImagContainsNull) {
            IJ.log("Failed to perform FHT of kernels.");
            show_error_msg();
            return;
        }

        ImageStack iStackReal = new ImageStack
            (impCombined.getWidth(), impCombined.getHeight(),
             aryFhtPadded.length * aryFhtKernelReal.length);
        ImageStack iStackAmp = new ImageStack
            (impCombined.getWidth(), impCombined.getHeight(),
             aryFhtPadded.length * aryFhtKernelReal.length);
        ImageStack iStackPhase = new ImageStack
            (impCombined.getWidth(), impCombined.getHeight(),
             aryFhtPadded.length * aryFhtKernelReal.length);
        Thread[] aryThreadMulInvAmpPhase = new Thread[numProcessors];
        for (int i = 0; i < numProcessors; i++) {
            int[] aryInd =
                new int[Math.floorDiv
                        (aryFhtPadded.length * aryFhtKernelReal.length
                         - 1 - i,
                         numProcessors) + 1];
            for (int j = 0; j < aryInd.length; j++) {
                aryInd[j] = i + j * numProcessors;
            }
            aryThreadMulInvAmpPhase[i] = new Thread
                (() -> {
                    for (int k = 0; k < aryInd.length; k++) {
                        int indPadded =
                            aryInd[k] / aryFhtKernelReal.length;
                        int indKernel =
                            aryInd[k] % aryFhtKernelImag.length;
                        FHT fhtConvolReal = aryFhtPadded[indPadded].
                            multiply(aryFhtKernelReal[indKernel]);
                        FHT fhtConvolImag = aryFhtPadded[indPadded].
                            multiply(aryFhtKernelImag[indKernel]);
                        fhtConvolReal.inverseTransform();
                        fhtConvolImag.inverseTransform();
                        fhtConvolReal.swapQuadrants();
                        fhtConvolImag.swapQuadrants();
                        fhtConvolReal.setRoi(xOffset, yOffset,
                                             impCombined.getWidth(),
                                             impCombined.getHeight());
                        fhtConvolImag.setRoi(xOffset, yOffset,
                                             impCombined.getWidth(),
                                             impCombined.getHeight());
                        FloatProcessor fpConvolReal =
                            (FloatProcessor) fhtConvolReal.crop();
                        FloatProcessor fpConvolImag =
                            (FloatProcessor) fhtConvolImag.crop();
                        FloatProcessor fpAmp = new FloatProcessor
                            (impCombined.getWidth(),
                             impCombined.getHeight());
                        FloatProcessor fpImag2 = new FloatProcessor
                            (impCombined.getWidth(),
                             impCombined.getHeight());
                        fpAmp.insert(fpConvolReal, 0, 0);
                        fpImag2.insert(fpConvolImag, 0, 0);
                        fpAmp.sqr();
                        fpImag2.sqr();
                        fpAmp.copyBits(fpImag2, 0, 0, Blitter.ADD);
                        fpAmp.sqrt();
                        FloatProcessor fpPhase = new FloatProcessor
                            (impCombined.getWidth(),
                             impCombined.getHeight());
                        for (int l = 0; l < impOriginal.getHeight();
                             l++) {
                            for (int m = 0;
                                 m < impOriginal.getWidth(); m++) {
                                float flReal =
                                    fpConvolReal.getPixelValue(m, l);
                                float flImag =
                                    fpConvolImag.getPixelValue(m, l);
                                fpPhase.putPixelValue
                                    (m, l,
                                     Math.atan2(flImag, flReal));
                            }
                        }
                        fpConvolReal.resetMinAndMax();
                        fpAmp.resetMinAndMax();
                        fpPhase.resetMinAndMax();
                        synchronized (iStackReal) {
                            iStackReal.setProcessor(fpConvolReal,
                                                    aryInd[k] + 1);
                            iStackAmp.setProcessor(fpAmp,
                                                   aryInd[k] + 1);
                            iStackPhase.setProcessor(fpPhase,
                                                     aryInd[k] + 1);
                        }
                    }
                });
            aryThreadMulInvAmpPhase[i].start();
        }
        for (int i = 0; i < numProcessors; i++) {
            try {
                aryThreadMulInvAmpPhase[i].join();
            } catch (InterruptedException e) {
                IJ.log("Thread has already been interrupted: " + i);
            }
        }
        boolean iStackRealAmpPhaseContainsNull = false;
        for (int i = 0; i < iStackReal.size(); i++) {
            if (iStackReal.getProcessor(i + 1) == null) {
                iStackRealAmpPhaseContainsNull = true;
                break;
            }
        }
        if (! iStackRealAmpPhaseContainsNull) {
            for (int i = 0; i < iStackAmp.size(); i++) {
                if (iStackAmp.getProcessor(i + 1) == null) {
                    iStackRealAmpPhaseContainsNull = true;
                    break;
                }
            }
        }
        if (! iStackRealAmpPhaseContainsNull) {
            for (int i = 0; i < iStackPhase.size(); i++) {
                if (iStackPhase.getProcessor(i + 1) == null) {
                    iStackRealAmpPhaseContainsNull = true;
                    break;
                }
            }
        }
        if (iStackRealAmpPhaseContainsNull) {
            IJ.log("Failed to perform inverse FHT.");
            show_error_msg();
            return;
        }
        for (int i = 0; i < iStackAmp.getSize(); i++) {
            int indPadded = i / aryFhtKernelReal.length;
            int indKernel = i % aryFhtKernelReal.length;
            String strLabel = "radius";
            if (this.outputType == ONE_FOR_MIN_TO_MAX) {
                strLabel = strLabel + "_" + radiusMin + "_"
                    + radiusMax;
            } else if (this.outputType == ONE_FOR_EACH_RADIUS) {
                strLabel = strLabel + "_" + (radiusMin + indKernel);
            } else if (this.outputType == CUMULATIVE_FROM_MIN) {
                strLabel = strLabel + "_" + radiusMin + "_"
                    + (radiusMin + indKernel);
            } else if (this.outputType == CUMULATIVE_FROM_MAX) {
                strLabel = strLabel + "_" + (radiusMin + indKernel)
                    + "_" + radiusMax;
            }
            strLabel = strLabel + "_frame_" + indPadded;
            iStackReal.setSliceLabel(strLabel, i + 1);
            iStackAmp.setSliceLabel(strLabel, i + 1);
            iStackPhase.setSliceLabel(strLabel, i + 1);
        }
        String titleOut = impCombined.getTitle();
        if (this.outputType == ONE_FOR_MIN_TO_MAX) {
            titleOut = titleOut + "_OneForMinToMax";
        } else if (this.outputType == ONE_FOR_EACH_RADIUS) {
            titleOut = titleOut + "_OneForEachRadius";
        } else if (this.outputType == CUMULATIVE_FROM_MIN) {
            titleOut = titleOut + "_CumulFromMin";
        } else if (this.outputType == CUMULATIVE_FROM_MAX) {
            titleOut = titleOut + "_CumulFromMax";
        }
        titleOut = titleOut + "_" + radiusMin + "_" + radiusMax;
        ImagePlus impReal;
        ImagePlus impAmp;
        ImagePlus impPhase;
        try {
            impReal =
                new ImagePlus(titleOut + "_Real", iStackReal);
            impAmp =
                new ImagePlus(titleOut + "_Amp", iStackAmp);
            impPhase =
                new ImagePlus(titleOut + "_Phase", iStackPhase);
        } catch (IllegalArgumentException e) {
            IJ.log("Failed to create result image.");
            show_error_msg();
            return;
        }
        if (1 < aryFhtPadded.length) {
            impReal = ij.plugin.HyperStackConverter.toHyperStack
                (impReal, 1, aryFhtKernelReal.length,
                 aryFhtPadded.length, "default", "Color");
            impAmp = ij.plugin.HyperStackConverter.toHyperStack
                (impAmp, 1, aryFhtKernelReal.length,
                 aryFhtPadded.length, "default", "Color");
            impPhase = ij.plugin.HyperStackConverter.toHyperStack
                (impPhase, 1, aryFhtKernelReal.length,
                 aryFhtPadded.length, "default", "Color");
        }
        IJ.run(impPhase, "Fire", "");
        IJ.resetMinAndMax(impReal);
        IJ.resetMinAndMax(impAmp);
        IJ.resetMinAndMax(impPhase);
        IJ.log("Saving image: " + impReal.getTitle());
        IJ.saveAs(impReal, "tif", this.saveDir + impReal.getTitle());
        IJ.log("Saving image: " + impAmp.getTitle());
        IJ.saveAs(impAmp, "tif", this.saveDir + impAmp.getTitle());
        IJ.log("Saving image: " + impPhase.getTitle());
        IJ.saveAs(impPhase, "tif",
                  this.saveDir + impPhase.getTitle());
        impReal.show();
        impAmp.show();
        impPhase.show();
    }
}
